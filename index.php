<!DOCTYPE html>
<html>
<head>
  <title>Exact Control</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="A Exact Control Equipamentos Industriais é um Distribuidor autorizado Bosch Rexroth e Anchor, Realizamos Manutenção e Montagem de Equipamentos Industriais">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Rotec, Anchor, Bosch Rexroth, Equipamentos, Industriais, Manutenção, Montagem">
  <meta name="author" content="Rafael Freitas">

  <link rel="icon" href="_imagens/_logos/icone.ico" type="image/x-icon" />
  <link rel="stylesheet" type="text/css" href="_css/main.css">
  <link rel="stylesheet" type="text/css" href="_css/animate.css">
  <link rel="stylesheet" type="text/css" href="_css/bootstrap-social.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="_plugins/owlcarousel/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="_plugins/owlcarousel/owl.theme.default.min.css">

  <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="_plugins/owlcarousel/owl.carousel.min.js"></script>
  <script async src="_js/picturefill.min.js"></script>
  <script async src="_js/main.js"></script>

  <script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>
  

  <script>

  //Evento para rolar a página.
  $(document).ready(function(){
    // Add scrollspy to <body>
    $('body').scrollspy({target: ".navbar", offset: 50});   

      // Add smooth scrolling on all links inside the navbar
    $("#myNavbar a").on('click', function(event) {
      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();
        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
        });
      }  // End if
    });
  });


  //Evento para redimensionar Carousel para Mobile(Ainda não testeado)

  if ($(window).width() < 960) {
    $('#myCarousel .hide-on-mobile').removeClass('carousel-caption').addClass('hide');
  }

  //Redimensionar Carousel - Caption
  $(document).ready(function() {
    $('.carousel .carousel-caption').css('zoom', $('.carousel').width()/1250);
  });

  $(window).resize(function() {
    $('.carousel .carousel-caption').css('zoom', $('.carousel').width()/1250);
  });


  //Evento para o Collapse dos serviços

  $(document).ready(function(){
    $("#demo1").on("hide.bs.collapse", function(){
      $(".demo1").html('<span class="glyphicon glyphicon-plus"></span> Manutenção');
    });
  
    $("#demo1").on("show.bs.collapse", function(){
      $(".demo1").html('<span class="glyphicon glyphicon-minus"></span> Manutenção');
      $('#demo2').collapse('hide');
      $('#demo3').collapse('hide');
      $('#demo4').collapse('hide');
    });
  });

  
  $(document).ready(function(){
    $("#demo2").on("hide.bs.collapse", function(){
      $(".demo2").html('<span class="glyphicon glyphicon-plus"></span> Montagem');
    });
  
    $("#demo2").on("show.bs.collapse", function(){
      $(".demo2").html('<span class="glyphicon glyphicon-minus"></span> Montagem');
      $('#demo1').collapse('hide');
      $('#demo3').collapse('hide');
      $('#demo4').collapse('hide');
    });
  });

  $(document).ready(function(){
    $("#demo3").on("hide.bs.collapse", function(){
      $(".demo3").html('<span class="glyphicon glyphicon-plus"></span> Outro Serviço(1)');
    });
  
    $("#demo3").on("show.bs.collapse", function(){
      $(".demo3").html('<span class="glyphicon glyphicon-minus"></span> Outro Serviço(1)');
      $('#demo1').collapse('hide');
      $('#demo2').collapse('hide');
      $('#demo4').collapse('hide');
    });
  });

  $(document).ready(function(){
    $("#demo4").on("hide.bs.collapse", function(){
      $(".demo4").html('<span class="glyphicon glyphicon-plus"></span> Outro Serviço(2)');
    });
  
    $("#demo4").on("show.bs.collapse", function(){
      $(".demo4").html('<span class="glyphicon glyphicon-minus"></span> Outro Serviço(2)');
      $('#demo1').collapse('hide');
      $('#demo2').collapse('hide');
      $('#demo3').collapse('hide');
    });
  });


  $(document).ready(function(){
    if ( $(window).width() < 580) {
      $('#logo').hide();
      $('#unidade').hide();
      
    }
  });


  $(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    stagePadding: 50,
    loop:true,
    margin:20,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
  });
});
/*
  $(document).ready(function(){
    $('.playVideo').click(function(){
      $('.playVideo').hide();
      $("#video1")[0].play();
    });
});

  $(document).ready(function(){
    $('.playVideo2').click(function(){
      $('.playVideo2').hide();
      $("#video2")[0].play();
    });
});

$(document).ready(function(){
    $('#painel video').click(function(){
      var vid = document.getElementById("video1"); 
      vid.pause();
      $('.playVideo').show();
    });
});

function pauseVideo(id){
  id.pause();
  if (id.id == "video1") {
    $('.playVideo').show();
  }if (id.id == "video2") {
    $('.playVideo2').show();
  }  
}
Sites
parker.com
fluipress.com

*/
$(document).ready(function(){
  $("#section5").css("height", $(window).height());
});

</script>

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

  <div id="section0" class="container-fluid" style="">
    <div style="float: right; margin-right: 30px;">
    <img src="_imagens/_logos/exactControl.png" id="logo1" class="animated fadeInDown">  
    <p id="textoInicio" class="text-right animated fadeInRight">Automação Industrial</p>  
    </div>

  </div>


  <nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="200" style="background-color: #31416B;"><!--696969-->
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="index.php"><img src="_imagens/_logos/exact2.png"></a>
      </div>
      <div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li><a href="#section1">Inicio</a></li>
            <li><a href="#section2">Nossa história</a></li>
            <li><a href="#section3">Produtos</a></li>
            <li><a href="#section4">Serviços</a></li>
            <li><a href="#section5">Fornecedores</a></li>
            <li><a href="#section6">Localização</a></li>
            <li><a href="#section7">Contato</a></li>
          </ul>
        </div>
      </div>
    </div>
  </nav>    

    <div id="section1" class="container-fluid">
      <!--div class="wrapper fix-margin-bottom">
      </div--><!-- /.wrapper -->
      <div class="container">
        <div id="carouselInicio" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
      
        <div class="item active">
          <img src="http://placehold.it/1200x400/cccccc/ffffff">
           <div class="carousel-caption">
            <h3>Headline</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. <a href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank" class="label label-danger">Bootstrap 3 - Carousel Collection</a></p>
          </div>
        </div><!-- End Item -->
 
         <div class="item">
          <img src="http://placehold.it/1200x400/999999/cccccc">
           <div class="carousel-caption">
            <h3>Headline</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. <a href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank" class="label label-danger">Bootstrap 3 - Carousel Collection</a></p>
          </div>
        </div><!-- End Item -->
        
        <div class="item">
          <img src="http://placehold.it/1200x400/dddddd/333333">
           <div class="carousel-caption">
            <h3>Headline</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. <a href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank" class="label label-danger">Bootstrap 3 - Carousel Collection</a></p>
          </div>
        </div><!-- End Item -->
        
        <div class="item">
          <img src="http://placehold.it/1200x400/999999/cccccc">
           <div class="carousel-caption">
            <h3>Headline</h3>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
          </div>
        </div><!-- End Item -->
                
      </div><!-- End Carousel Inner -->


      <ul class="nav nav-pills nav-justified">
          <li data-target="#carouselInicio" data-slide-to="0" class="active"><a href="#">About<small>Lorem ipsum dolor sit</small></a></li>
          <li data-target="#carouselInicio" data-slide-to="1"><a href="#">Projects<small>Lorem ipsum dolor sit</small></a></li>
          <li data-target="#carouselInicio" data-slide-to="2"><a href="#">Portfolio<small>Lorem ipsum dolor sit</small></a></li>
          <li data-target="#carouselInicio" data-slide-to="3"><a href="#">Services<small>Lorem ipsum dolor sit</small></a></li>
        </ul>


    </div><!-- End Carousel -->
      </div>


    
    </div> <!--section 1-->

    <div id="section2" class="container-fluid">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
          <li data-target="#myCarousel" data-slide-to="4"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

          <div class="item active">
            <img class="slide_carousel animated bounceInDown" src="_imagens/_carousel/bosch_rexroth.png" alt="First slide">
            <div class="carousel-caption hide-on-mobile">
              <h1 class="slide_carousel animated bounceInDown">Distribuidor Autorizado</h1>
              <p class="slide_carousel animated bounceInRight">Parceiros comprometidos com a qualidade.</p>
            </div>
          </div>

          <div class="item">
            <img class="slide_carousel animated fadeIn" src="_imagens/_carousel/rexroth1.jpg" alt="Second slide">
            <div class="carousel-caption hide-on-mobile" style="top:30px!important;">
              <h2 class="slide_carousel2 animated fadeIn" style="color: #000">Grande experiência no mercado de<br>Hidráulica Industrial</h2>
              <p class="slide_carousel2 animated fadeIn"><a class="btn btn-lg btn-warning" href="#" role="button">Leia mais</a></p>
            </div>
          </div>

          <div class="item">
            <img class="third-slide" src="_imagens/_carousel/anchor.jpg" alt="Third slide">
            <div class="carousel-caption hide-on-mobile" style="left: 40px!important; right: auto; margin-bottom: 120px;">
              <h2 class="slide_carousel animated fadeInLeftBig">Garantia de qualidade em<br>Mangueiras Hidráulicas e<br>Conexões.</h2>
            </div>
          </div>

          <div class="item">
            <img class="third-slide" src="_imagens/_carousel/enerpac.jpg" alt="Third slide">
            <div class="carousel-caption hide-on-mobile">
              <h1 class="slide_carousel animated fadeInDownBig">ENERPAC - Macacos Hidráulicos.</h1>
            </div>
          </div>

          <div class="item">
            <img class="third-slide" src="_imagens/_carousel/FennerDrivers.jpg" alt="Third slide">
            <div class="carousel-caption hide-on-mobile" style="margin-bottom: 40px; color: #014584">
              <h2 class="slide_carousel animated fadeInLeftBig">Elimine o impacto da formação<br>de eletrostática.</h2>
            </div>
          </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>

    </div> <!--section 2-->


    <div id="section3" class="container-fluid">

      <div class="container" style="margin-bottom: 50px; margin-top: 50px;">
        <div class="row">
            <div class="col-md-12">
              <div class="head">
                 <p class="text-center" id="quemsomos_tit">Nossa Historia</p>
              </div><!-- /.head -->
            </div><!-- /.col-md-12 -->

            <div class="col-md-6 col-sm-6">
              <div class="painel">
               <p id="quemsomos_des">A Rotec foi fundada em 1994 e vem atuando no mercado industrial tendo como base de um grande negócio, a qualidade e o bom relacionamento com parceiros e clientes.</p>
               <p id="quemsomos_des">Com uma área construída de 400 m² em Pernambuco,  a Rotec tem capacidade para executar reparos, montagem, manutenção e total assistência técnica em quaisquer equipamentos hidráulicos e pneumáticos Bosch Rexroth e Enerpac com garantia de fábrica.Buscando sempre a melhoria de seus serviços, a Rotec conta ainda com uma frota para agilizar sua equipe de atendimento e vendas. Após uma história de duas décadas de sucesso atuando no estado do Espírito Santo e no sul da Bahia, a Rotec Equipamentos Industriais Ltda amplia ainda mais seus horizontes com a inauguração da Rotec Equipamentos Industriais do Nordeste Ltda.</p>
              </div><!-- /painel -->
            </div><!-- /.col-md-3 col-sm-4 -->
          
            <div class="col-md-6 col-sm-6">
              <div class="painel">
                <img src="_imagens/progresso.png" class="img-responsive" alt="Cinque Terre" id="imgNossaHistoria" >     
              </div><!-- /painel -->
            </div><!-- /.col-md-3 col-sm-4 -->
                        
          </div><!-- /.row -->

        
      </div>
    </div> <!--section 3-->
    
    <div id="section4" class="container-fluid">
      
      <div id="servicos_topo">
      <p class="text-center"><img sizes="(min-width: 1em) 72vw"
          srcset="_imagens/_services/servicos_pq.jpg 300w,
          _imagens/_services/servicos_md.jpg 592w,
          _imagens/_services/servicos_gr.jpg 792w"
          alt="…"></p>
    
      </div>
      <script>
        jQuery(function(){
          jQuery('#botao_collapse').click();
        });
      </script>
     <div class="container">
        <button type="button" id="botao_collapse" class="btn btn-primary demo1" data-toggle="collapse" data-target="#demo1">
            <span class="glyphicon glyphicon-plus"></span> Manutenção
          </button>
          <button type="button" class="btn btn-primary demo2" data-toggle="collapse" data-target="#demo2">
            <span class="glyphicon glyphicon-plus"></span> Montagem
          </button>
          <button type="button" class="btn btn-primary demo3" data-toggle="collapse" data-target="#demo3">
            <span class="glyphicon glyphicon-plus"></span> Outro Serviço(1)
          </button>
          <button type="button" class="btn btn-primary demo4" data-toggle="collapse" data-target="#demo4">
            <span class="glyphicon glyphicon-plus"></span> Outro Serviço(2)
          </button>

        <div id="demo1" class="collapse">
          <div class="row">
            <div class="col-xs-6">
              <img src="_imagens/_services/manutencao.jpg" class="img-thumbnail" alt="Cinque Terre">     
            </div>
            <div class="col-xs-6">
              <p id="pservicos" align="justify">A Rotec atua em todo o setor industrial, com mão-de-obra especializada e atendimento individualizado para análise, manutenção e execução de serviços. A parceria com as marcas representadas garante ampla capacidade de atender com rapidez e eficiência os clientes, assegurando seu processo produtivo através de um estoque variado de peças.</p>    
            </div>
          </div>
        </div>

        <div id="demo2" class="collapse">
          <div class="row">
            <div class="col-xs-6">
              <p id="pservicos" align="justify">A Rotec oferece experiência em desenvolvimento de produtos que atendam a necessidade individual do cliente oferecendo assim, melhor eficiência, segurança e desempenho.</p>    
            </div>
            <div class="col-xs-6">
              <img src="_imagens/_services/montagem.jpg" class="img-thumbnail" alt="Cinque Terre">     
            </div>
          </div>
        </div>

        <div id="demo3" class="collapse">
          <div class="row">
            <div class="col-xs-6">
              <img src="_imagens/_services/outro1.png" class="img-thumbnail" alt="Cinque Terre">
            </div>
            <div class="col-xs-6">
              <p id="pservicos" align="justify">Aqui será descrito outro serviço efetuado pela empresa.</p>       
            </div>
          </div>
        </div>

        <div id="demo4" class="collapse">
          <div class="row">
            <div class="col-xs-6">
              <p id="pservicos" align="justify">Aqui será descrito outro serviço efetuado pela empresa.</p>    
            </div>
            <div class="col-xs-6">
              <img src="_imagens/_services/outro1.png" class="img-thumbnail" alt="Cinque Terre">    
            </div>
          </div>
        </div> 

      </div>
     
    </div> <!--section 4-->

      <div id="section5" class="container-fluid">
        <div id="produtos_topo">
        <h1>Produtos</h1>
        <p>Em desenvolvimento. Em desenvolvimento.</p>
        <p>Em desenvolvimento. Em desenvolvimento.</p>
        </div>
        <div class="container">
          <div class="owl-carousel owl-theme" id="owl-produtos">
            <div class="item"><img src="_imagens/_owlcarousel/01.jpg" class="img-thumbnail"><h4>Bomba Hidráulica</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/02.jpg" class="img-thumbnail"><h4>Válvula de Esfera - Alta Pressão</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/03.jpg" class="img-thumbnail"><h4>Comandos Hidráulicos</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/04.jpg" class="img-thumbnail"><h4>Acoplamentos Elásticos</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/05.jpg" class="img-thumbnail"><h4>Bomba Manual 700bar</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/06.jpg" class="img-thumbnail"><h4>Correias de Transmissão Fenner Drives</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/07.jpg" class="img-thumbnail"><h4>Engate Rápido</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/08.jpg" class="img-thumbnail"><h4>Correias de Alta Performance Fenner Drives</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/09.jpg" class="img-thumbnail"><h4>Macacos Hidráulicos Enerpac</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/10.jpg" class="img-thumbnail"><h4>Braçadeiras Bipartidas</h4></div>
            <div class="item"><img src="_imagens/_owlcarousel/11.jpg" class="img-thumbnail"><h4>Filros Hidráulicos de Alta Performance</h4></div>
          </div>
          <p id="descricao-indrodutoria2" align="justify" class="recuo">A empresa entende que o melhor caminho para um alto rendimento está em reduzir o fator desperdício do processo de automação projetando equipamentos com inteligência e buscando continuamente a melhoria do sistema de gestão da qualidade.<br>
          </p>
          <p id="descricao-indrodutoria2" align="justify" class="recuo" style="margin-bottom: 50px;">Especializada em projetos, reposição de sobressalentes, manutenção e assistência técnica em equipamentos hidráulicos e pneumáticos, a Rotec é distribuidora exclusiva para as regiões do Espírito Santo e Sul da Bahia, Pernambuco, Alagoas e Paraíba das marcas Bosch Rexroth e Gummi, sendo distribuidor nacional para as marcas Enerpac, Hallite, Larga, Mangueiras Anchor Caterpillar, Fenner Drives, Gemels e Aegi Clamps consolidadas em todo o mundo por seu excepcional padrão de qualidade.</p>
          
        </div><!-- / .container-->

      </div> <!--section 5-->

      <div id="section6" class="container-fluid">
        <div id="localizacao_topo">
          <h2 style="color: #000; text-align: center; padding: 0; margin: 0;">Onde estamos</h2>
        </div>

        <div id="divmaps">
          <iframe id="maps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3949.930241348024!2d-34.90789628512807!3d-8.108585794162552!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab1f003c4ff01d%3A0xb90a0b085ce9bcd6!2sRotec+Equipamentos+Industriais+do+Nordeste+LTDA+(Rotec+NE)!5e0!3m2!1spt-BR!2sbr!4v1488794993431" height="450" style="border:2px solid lightgrey;" allowfullscreen></iframe>
          
<!-- Endereço Velho src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7899.737829380891!2d-34.92809863957007!3d-8.114827179534737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab1e5c5a48a9d9%3A0x230913b639185dd9!2sRotec+Equipamentos+Industriais!5e0!3m2!1spt-BR!2sbr!4v1488376894600"></iframe-->

          <div id="endereco">
            <p align="center">Rua Srg. Silvino de Macedo Nº 208</p>
            <p align="center">Imbiribeira - Recife - PE</p>
            <p align="center">CEP: 51160-060</p>
            <p align="center">(81) 3471-6198</p>
            <p align="center">rotec@rotec-ne.com.br</p>
          </div>
              
          
        </div><!--divmaps-->
      </div><!--section6-->


      <div id="section7" class="container-fluid">
        <div id="formulario">
        <h2 style="margin-left: 25px;">Fale Conosco</h2>
        <p style="margin-left: 25px;">Caso deseje entrar em contato, preencha o formulário abaixo.</p>
        <form class="form-horizontal" method="POST" action="#" style="max-width: 800px;">
          <div class="form-group">
            <label class="control-label col-sm-2" for="nome">Nome:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe seu nome" required>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="empresa">Empresa:</label>
            <div class="col-sm-10"> 
              <input type="text" class="form-control" id="empresa" name="empresa" placeholder="Qual empresa você representa?" required>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="telefone">Telefone:</label>
            <div class="col-sm-10">
              <input pattern="^\d{2}-\d{5}-\d{4}$" type="tel" class="form-control" rows="3" id="telefone" name="telefone" OnKeyPress="formatar('##-#####-####', this)" maxlength="13" placeholder="00-00000-0000" style="max-width: 200px;" required></input>Caso deseje informar um Nº Fixo, colocar um 0 no lugar do 9º dígito.
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="email" name="email" placeholder="E-mail para contato." pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="exemplo@exemplo.com" required>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="comentario">Descrição:</label>
            <div class="col-sm-10">
              <textarea class="form-control" rows="3" id="comentario" name="comentario" placeholder="Descreva sua necessidade:" required></textarea>
            </div>
          </div>
          <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default">Enviar</button>
            </div>
          </div>
        </form>
        </div>
      </div>

      <div id="section8" class="container-fluid">
      <div id="footer">
        <p class="text-center">ROTEC<br>Equipamentos Industriais</p>
        <a class="btn btn-block btn-social btn-facebook" id="socialf" href="https://www.facebook.com/rotecne/?fref=ts" target="_blank">
          <span class="fa fa-facebook"></span> Siga-nos no Facebook
        </a>
        <a class="btn btn-block btn-social btn-linkedin" id="socialf" href="https://br.linkedin.com/company/rotec---equipamentos-industriais-ltda." target="_blank">
          <span class="fa fa-linkedin"></span> Siga-nos no LinkedIn
        </a>
      </div>
      </div>

<!--

<script type="text/javascript">
      //Redimensionar Carousel - Caption
      $(document).ready(function() {
        $('#section4 .conteudo_servicos').css('zoom', $('#section4').width()/950);
      });

      $(window).resize(function() {
        $('#section4 .conteudo_servicos').css('zoom', $('#section4').width()/950);
      });
    </script>


<h1 style="text-align: center;"><span class="glyphicon glyphicon-cog"></span> Nossos Serviços <span class="glyphicon glyphicon-wrench"></h1>

-->
</body>
</html>